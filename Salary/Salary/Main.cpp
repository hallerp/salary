
// Assignment # 4 Salary
// Paul Haller
// EXTRA CREDIT ATTEMPTED

#include <iostream>
#include <conio.h>
#include <iomanip>

using namespace std;

struct EmployeeData
{
	int Id;
	string FirstName;
	string LastName;
	float PayRate;
	float Hours;
};

void EnterData(EmployeeData *employee, int people)
{
	for (int i = 0; i < people; i++)
	{
		cout << "Please enter employee ID for employee " << (i + 1) << ":\n";
		cin >> employee[i].Id;
		cout << "Please enter employee first name for employee " << (i + 1) << ":\n";
		cin >> employee[i].FirstName;
		cout << "Please enter employee last name for employee " << (i + 1) << ":\n";
		cin >> employee[i].LastName;
		cout << "Please enter employee payrate for employee " << (i + 1) << ":\n";
		cin >> employee[i].PayRate;
		cout << "Please enter employee hours for employee " << (i + 1) << ":\n";
		cin >> employee[i].Hours;
	}
}

void PrintData(EmployeeData *employee, int people)
{
	float totalPay = 0;
	float grossPay = 0;
	for (int i = 0; i < people; i++)
	{
		grossPay = employee[i].PayRate * employee[i].Hours;
		cout << "Employee #" << employee[i].Id << " " << employee[i].FirstName << " " << employee[i].LastName << " $" << setprecision(2) << fixed << grossPay << "\n\n";
		totalPay += grossPay;
	}

	cout << "Total Gross Pay $" << setprecision(2) << fixed << totalPay;
}

int main()
{
	int people;
	cout << "How many employees?\n";
	cin >> people;

	EmployeeData *employee = new EmployeeData[people];

	EnterData(employee, people);

	PrintData(employee, people);


	(void)_getch();
	return 0;
}